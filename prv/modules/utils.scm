(define-module (modules utils)
  #:use-module (artanis artanis)
  #:use-module (artanis utils)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-19)
  #:use-module (web uri)

  #:use-module (modules settings)
  #:use-module (modules file-uploads)

  #:export (conn
            mtable
            init-database
            show-boards
            send-textfile
            default
            read-file
            bv->alist
            split-form
            comment-filter
            replace
            view-replies
            print-threads
            build-board
            build-thread
            post-to-board
            post-to-thread
            delete-post))

(define conn #f)
(define mtable #f)

(define (get-timestamp13)
  (let ((ctime (current-time time-utc)))
    (inexact->exact
      (floor
        (+ (* (time-second ctime) 1000.)
           (/ (time-nanosecond ctime) 1000000.))))))

(define (get-ip rc)
  (let* ((request ((record-accessor (record-type-descriptor rc) 'request) rc))
         (headers ((record-accessor (record-type-descriptor request) 'headers) request))
         (ip (assoc-ref headers 'x-forwarded-for))
         (ip-cf (assoc-ref headers 'cf-connecting-ip)))
    (cond
      (ip-cf ip-cf)
      (ip ip)
      (else "none"))))
(define (get-session rc)
  (let ((session (get-from-qstr rc "session")))
    (if session
      session
      (get-ip rc))))

(define (check-cooldown ip)
  (let* ((ip (default ip "none"))
         (temp (mtable 'get 'cooldowns #:condition (where #:ip ip)))
         (ctime (time-second (current-time time-utc))))
    (if (null? temp)
      (begin
        (mtable 'set 'cooldowns #:ip ip #:tier1 ctime #:tier2 ctime #:counter 1)
        (cons #t ""))
      (let ((tier1 (assoc-ref (car temp) "tier1"))
            (tier2 (assoc-ref (car temp) "tier2"))
            (tier2-counter (assoc-ref (car temp) "counter")))
        (if (< ctime (+ tier2 tier2-cooldown))
          (if (>= tier2-counter tier2-post-limit)
            (cons #f (format #f "[0;31mError: You have made ~a posts in the last ~a seconds\nwhich is more than the configured maximum.\nPlease wait ~a more seconds and try posting again.[0m" tier2-counter tier2-cooldown (- (+ tier2 tier2-cooldown) ctime)))
            (if (< ctime (+ tier1 tier1-cooldown))
              (cons #f (format #f "[0;31mError: Post cooldown - ~a seconds, ip: ~a[0m" (- (+ tier1 tier1-cooldown) ctime) ip))
              (begin
                (mtable 'set 'cooldowns (format #f "tier1=~a,counter=counter+1 where ip='~a'" ctime ip))
                (cons #t ""))))
          (if (< ctime (+ tier1 tier1-cooldown))
            (cons #f (format #f "[0;31mError: Post cooldown - ~a seconds[0m" (- (+ tier1 tier1-cooldown) ctime)))
            (begin
              (mtable 'set 'cooldowns (format #f "tier1=~a,tier2=~a,counter=1 where ip='~a'" ctime ctime ip))
              (cons #t ""))))))))

(define (init-database)
  ((mtable 'create 'cooldowns '((ip text)
                                (tier1 integer)
                                (tier2 integer)
                                (counter integer))
           #:if-exists? 'ignore #:engine #f)
   'valid?)
  ((mtable 'create 'replies '((session text)
                              (since integer)
                              (replies text))
           #:if-exists? 'ignore #:engine #f)
   'valid?)
  ((mtable 'create 'boards '((name text)
                             (ctime integer)
                             (btime integer)
                             (threadnum integer))
           #:if-exists? 'ignore #:engine #f)
   'valid?)
  ((mtable 'create 'threads '((id integer (#:primary-key))
                              (subject text)
                              (board text)
                              (date text)
                              (ctime integer)
                              (btime integer)
                              (replies integer)
                              (session text)
                              (watchers text))
           #:if-exists? 'ignore #:engine #f)
   'valid?)
  ((mtable 'create 'mods '((id integer (#:primary-key))
                           (ctime integer)
                           (name text)
                           (password text)
                           (permissions text))
           #:if-exists? 'ignore #:engine #f)
   'valid?))

(define* (send-text response #:optional (status 200))
  (let* ((response-length (bytevector-length (string->bytevector response "utf8"))))
    (response-emit response #:status status #:headers `((content-type . (text/plain))
                                                     (content-length . ,response-length)))))
(define (send-textfile file)
  (send-text (read-file (string-append "pub/" file))))

(define (show-boards rc)
  (string-append
    "\n[1;33mBoard List:[0m\n"
    (string-join
      (map (lambda (brd)
             (format #f " [1;34m/~a/ [0;36m- ~a[0m" (car brd) (cadr brd)))
           board-list)
      "\n")
    "\n\n"))
  ;; KEEP THESE - in the future make an if statement
  ;;   if users can post to arbitrary boards (and create them)
  ;;   then use the code below, otherwise the code above
  ;(let ((boards (mtable 'get 'boards #:columns '(name))))
  ;  (string-append
  ;    "\n[0;31mList of active boards:[0m\n\n"
  ;    (string-join
  ;      (map (lambda (brd) (string-append "[1;34m" (default (assoc-ref brd "name") "") "[0m"))
  ;           boards)
  ;      "\n")
  ;    "\n\n")))

(define (default option def)
  (if (or (eq? option #f)
          (null? option)
          (equal? option ""))
    def
    option))
(define (read-file filename)
  (with-input-from-file filename
    (lambda ()
      (let loop ((ls1 '()) (c (read-char)))
        (if (eof-object? c)
          (list->string (reverse ls1))
          (loop (cons c ls1) (read-char)))))))
(define (bv->alist bv)
  (if (not bv)
    '()
    (let ((str (utf8->string bv))
          (head "\r\nContent-Disposition: form-data; name=\""))
      ;(format #t "\n\nBYTEVECTOR-STRING=[~a]\n\n" str)
      (let ((test (string-contains str head)))
        (if (not test)
          (begin (display "ERROR READING FORM DATA\n") '())
          (split-form str (substring str 0 test) (string-append (substring str 0 test) head)))))))
(define (split-form form-str token separator)
  (let loop ((str form-str)
             (idx (string-length token)))
    (if (not (string-prefix? separator str))
      '()
      (let* ((key (substring str (string-length separator) (string-index str #\" (string-length separator))))
             (end (string-contains str token idx)))
        (cons (cons (string->symbol key)
                    (substring str (+ (string-length separator) (string-length key) 5) (- end 2)))
              (loop (substring str end)
                    idx))))))
(define (comment-filter comment board thread id)
  (let ((filters (list
                   (lambda (p)
                     (regexp-substitute/global #f "\r\n" p
                       'pre "\n" 'post))
                   (lambda (p)
                     (regexp-substitute/global #f "(http|https|ftp)://[^\n ]+" p
                       'pre (lambda (m) (string-append "[0;34m" (match:substring m) "[0m")) 'post)) ; FIXME : add quotes around the href and target once you figure out how to escape chars properly
                   (lambda (p)
                     (regexp-substitute/global #f ">>[0-9]+" p
                       ;; FIXME : ids currently have a p suffix, this needs to be a p prefix, but there is some difficulty with the templating
                       'pre (lambda (m)
                              (alert-reply board thread (string->number (substring (match:substring m) 2)) id)
                              (string-append "[0;31m" (match:substring m) "[0m")) 'post)) ; FIXME : add quotes around the href once you figure out how to escape chars properly
                   (lambda (p)
                     (regexp-substitute/global #f "(^|\n)>[^\n]+" p
                       'pre (lambda (m) (string-append "[0;32m" (match:substring m) "[0m")) 'post))
                   (lambda (p)
                     (regexp-substitute/global #f "\\[spoiler\\].*\\[/spoiler\\]" p
                       'pre (lambda (m) (string-append "[0;47;37m" (substring (match:substring m) 9 (- (string-length (match:substring m)) 10)) "[0m")) 'post)))))
    (let next-filter ((final (replace (replace comment "" "") "\"" "＂")) ; FIXME: This quote replacement should NOT be needed, but quotation marks won't be inserted into the database
                      (flist filters))
      (if (null? flist)
        final
        (next-filter ((car flist) final) (cdr flist))))))

(define (alert-reply board thread to from)
  (let* ((session (assoc-ref (car (mtable 'get (string->symbol (string-append "thread" thread))
                                          #:columns '(session) #:condition (where #:id to)))
                             "session"))
         (temp (mtable 'get 'replies #:columns '(replies) #:condition (where #:session session))))
    (if (null? temp)
      (mtable 'set 'replies #:session session #:since (time-second (current-time time-utc)) #:replies (format #f "~a/~a/~a" board thread from))
      (let ((replies (assoc-ref (car temp) "replies")))
        (mtable 'set 'replies (format #f "replies='~a' where session='~a'"
                                      (format #f "~a~a~a/~a/~a" replies (if (equal? replies "") "" ",") board thread from)
                                      session))))))

(define (view-replies rc)
  (let* ((session (get-session rc))
         (temp (mtable 'get 'replies #:columns '(replies) #:condition (where #:session session)))
         (replies (if (null? temp)
                    ""
                    (assoc-ref (car temp) "replies")))
         (clear (get-from-qstr rc "clear"))
         (response (format #f "\n[1;33mReplies to your posts:\n[1;31m ~a[0m\n\n" (string-join (string-split replies #\,) "\n "))))
    (when (equal? clear "y")
      (mtable 'set 'replies (format #f "replies='' where session='~a'" session)))
    (send-text response)))

(define (replace str old new) ; FIXME: This function is shit, and shouldn't be needed anyway
  (let loop ((str str)
             (idx 0))
    (let ((jdx (string-contains str old idx)))
      (if jdx
        (loop (string-append
                (substring str 0 jdx)
                new
                (substring str (+ jdx (string-length old)) (string-length str)))
              (+ jdx (string-length new)))
        str))))

(define (print-threads board threadnum preview)
  (let ((temp (mtable 'get 'threads #:columns '(subject date) #:condition (where #:id threadnum))))
    (if (null? temp)
      ""
      (let* ((threadname (string-append "thread" (number->string threadnum)))
             (subject (assoc-ref (car temp) "subject"))
             (tdate (assoc-ref (car temp) "date")))
             ;(subject (cdaar (mtable 'get 'threads #:columns '(subject) #:condition (where #:id threadnum)))))
             ;(maxid (if preview (assoc-ref (car (mtable 'get (string->symbol threadname) #:columns '(id) #:functions '((max id)))) "id")
             ;                   0))) ; Save a database call, maxid isn't needed unless it's a preview
        (if preview
          (format #f "[1;31m【~a】[1;35m~a [0;36m~a" threadnum subject tdate)
          (string-append
            (format #f "[1;31m【~a】[1;35m~a\n[1;35m━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━\n" threadnum subject)
            (string-join
              (map
                (lambda (post)
                  (let ((postnum (assoc-ref post "id"))
                        (name (assoc-ref post "name"))
                        (date (assoc-ref post "date"))
                        ;(image (assoc-ref post "image"))
                        ;(thumb (assoc-ref post "thumb"))
                        ;(iname (assoc-ref post "iname"))
                        ;(size (assoc-ref post "size"))
                        (comment (replace (replace (replace (replace (replace (assoc-ref post "comment") "\\u3000" "　") "\\\\" "\\") "＂" "\"") "\\n" "\n") "\\x1b" ""))) ; FIXME: replace shouldn't be needed
                    ;(if (= postnum 1)
                    ;  (tpl->response "pub/post-OP.tpl" (the-environment))
                    ;  (tpl->response "pub/post.tpl" (the-environment)))))
                    (format #f "[1;33m~a. [1;32m~a [0;36m~a[0m\n~a\n" postnum name date comment)))
                ;(if preview
                ;  (mtable 'get (string->symbol threadname) #:condition (where (format #f "id=1 or id>~a-~a" maxid post-preview-count)))
                (mtable 'get (string->symbol threadname)))
              "\n")))))))

(define (build-board rc)
  (let ((board (params rc "first")))
    (let ((contents (string-join
                      ;; FIXME: Replace this sort method with the #:order-by parameter which doesn't seem to be working yet
                      (let ((listing (sort (mtable 'get 'threads #:condition (where #:board board))
                                       (lambda (A B) (< (assoc-ref A "ctime") (assoc-ref B "ctime"))))))
                        (map (lambda (n thread)
                               (print-threads board (assoc-ref thread "id") #t))
                             (iota (length listing))
                             listing))
                      "\n")))
      (send-text (string-append "\n"
                                (default contents
                                  (string-append "[0;31mBoard ([0;32m" board "[0;31m) is currently empty[0m"))
                                "\n\n")))))

(define (build-thread rc)
  (let* ((board (params rc "first"))
         (threadnum (params rc "second"))
         (contents (print-threads board (string->number threadnum) #f)))
    (if (equal? contents "")
      (send-text (string-append "\n[0;31mError 404: Thread ([0;32m/" board "/" threadnum "[0;31m) not found[0m\n\n") 404)
      (send-text (string-append "\n" contents "\n")))))

(define (field-too-long? name field max-length max-lines)
  (cond
    ((> (string-length field) max-length)
     (format #f "\n[0;31mError 400: ~a field length too large ([0;33m~a/~a[0;31m characters used)[0m\n\n" name (string-length field) max-length))
    ((> (length (string-split field #\newline)) max-lines)
     (format #f "\n[0;31mError 400: ~a field has too many lines ([0;33m~a/~a[0;31m lines used)[0m\n\n" name (length (string-split field #\newline)) max-lines))
    (else #f)))

(define (post-to-board rc)
  (let ((ip-addr (get-ip rc)))
    (if (and require-ip-header
             (equal? ip-addr "none"))
      (send-text (format #f "\n[0;31mError 401: ip header not found[0m\n\n") 401)

      (let ((board (params rc "board"))
            (cooldown (check-cooldown ip-addr)))
        (if (not (car cooldown))
          (send-text (format #f "\n~a\n\n" (cdr cooldown)))
          (if (assoc-ref board-list board)
            (let* ((data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                                     (rc-body rc)))
                   (subject (default (assoc-ref data 'subject) default-title))
                   (name (default (assoc-ref data 'name) (cadr (assoc-ref board-list board))))
                   (date (date->string (current-date 0) "~5"))
                   (ctime (time-second (current-time time-utc)))
                   (threadnum (+ 1 (default (assoc-ref
                                             (car (mtable 'get 'threads #:columns '(id) #:functions '((max id))))
                                             "id")
                                     0)))
                   (comment (comment-filter (default (assoc-ref data 'comment) "") board threadnum 1))
                   (tname (string->symbol (string-append "thread" (number->string threadnum))))
                   (session (default (assoc-ref data 'session) ip-addr)))

              (let ((too-long (default (field-too-long? 'subject subject max-subject-length 1)
                                       (default (field-too-long? 'name name max-name-length 1)
                                                (default (field-too-long? 'comment comment max-comment-length max-comment-lines)
                                                         (field-too-long? 'session session max-session-length 1))))))

                (if too-long
                  (send-text too-long 400)

                  (begin
                    (mtable 'set 'threads #:subject subject #:board board #:date date #:ctime ctime #:btime ctime #:replies 1 #:session "" #:watchers "")

                    ((mtable 'create tname '((id integer) ;(#:primary-key) not working after init-server for some reason
                                             (ip text)
                                             (session text)
                                             (name text)
                                             (ctime integer)
                                             (date text)
                                             (comment text)
                                             (subs text))
                             #:if-exists? 'ignore #:engine #f)
                     'valid?)

                    (mtable 'set tname #:id 1 #:ip ip-addr #:session session #:name name #:ctime ctime #:date date #:comment comment #:subs "")
                    (format #t "\n\nWEEEEEEEEEEEEEEEEEEEE: ~a\n\n" tname)

                    (if (null? (mtable 'get 'boards #:condition (where #:name board)))
                      (mtable 'set 'boards #:name board #:ctime ctime #:btime ctime #:threadnum threadnum)
                      (mtable 'set 'boards (format #f "btime=~a,threadnum=~a where name='~a'" ctime threadnum board)))

                    (send-text (format #f "\n[0;31mThread [0;32m#~a [0;31mposted successfully on board [0;32m~a[0m\n\n" threadnum board))))))
            (send-text (format #f "\n[0;31mError 404: Board [0;32m~a [0;31mnot found[0m\n\n" board) 404)))))))

(define (post-to-thread rc)
  (let ((ip-addr (get-ip rc)))
    (if (and require-ip-header
             (equal? ip-addr "none"))
      (send-text (format #f "\n[0;31mError 401: ip header not found[0m\n\n") 401)

      (let ((cooldown (check-cooldown ip-addr)))
        (if (not (car cooldown))
          (send-text (format #f "\n~a\n\n" (cdr cooldown)))
          (let* ((board (params rc "board"))
                 (threadnum (params rc "thread"))
                 (data (parse-body (string->utf8 (string-append "--" (content-type-is-mfd? rc))) ; FIXME: pare-body is our own function, there should be a built-in one
                                   (rc-body rc)))
                 (name (default (assoc-ref data 'name) (cadr (assoc-ref board-list board))))
                 (date (date->string (current-date 0) "~5"))
                 (ctime (time-second (current-time time-utc)))
                 (btime (time-second (current-time time-utc)))
                 ;(comment (default (comment-filter (assoc-ref data 'comment) board threadnum) default-comment))
                 (tname (string->symbol (string-append "thread" threadnum)))
                 (id (+ 1 (assoc-ref (car (mtable 'get tname #:columns '(id) #:functions '((max id)))) "id")))
                 (comment (comment-filter (assoc-ref data 'comment) board threadnum id))
                 (session (default (assoc-ref data 'session) ip-addr)))

            (let ((too-long (default (field-too-long? 'name name max-name-length 1)
                                     (default (field-too-long? 'comment comment max-comment-length max-comment-lines)
                                              (field-too-long? 'session session max-session-length 1)))))
              (if too-long
                (send-text too-long 400)
                (begin
                  (mtable 'set tname #:id id #:ip ip-addr #:session session #:name name #:ctime ctime #:date date #:comment comment)
                  (mtable 'set 'threads (format #f "btime=~a,replies=replies+1 where id=~a" btime threadnum))

                  (send-text (format #f "\n[0;31mPost [0;32m#~a [0;31msuccessfully made in thread [0;32m/~a/~a[0m\n\n" id board threadnum)))))))))))

(define (delete-post rc)
  (let ((ip-addr (get-ip rc)))
    (if (and require-ip-header
             (equal? ip-addr "none"))
      (send-text (format #f "\n[0;31mError 401: ip header not found[0m\n\n") 401)

      (let ((cooldown (check-cooldown ip-addr)))
        (if (not (car cooldown))
          (send-text (format #f "\n~a\n\n" (cdr cooldown)))
          (let* ((board (params rc "board"))
                 (threadnum (params rc "thread"))
                 (postnum (params rc "post"))
                 (dtime (time-second (current-time time-utc)))
                 (data (mtable 'get 'threads #:columns '(board) #:condition (where #:id (string->number threadnum)))))
            (if (not (equal? (assoc-ref (car data) "board") board))
              (send-text (format #f "\n[0;31mWarning: thread not found on this board, please make sure your parameters are correct[0m\n\n") 400)

              (let* ((temp (car (mtable 'get (string->symbol (string-append "thread" threadnum)) #:columns '(ip ctime) #:condition (where #:id postnum))))
                     (ip (assoc-ref temp "ip"))
                     (ctime (assoc-ref temp "ctime")))

                (if (or (not ctime)
                        (not (equal? ip ip-addr))
                        (< (+ ctime deletion-period) dtime))
                  (send-text (format #f "\n[0;31mError 401: Cannot delete post - Either you are not the post owner or the deletion period [0;32m(~a seconds)[0;31m has expired[0m\n\n" deletion-period) 401)
                  (begin
                    (if (equal? postnum "1")
                      (begin
                        (DB-query conn (format #f "drop table thread~a" threadnum))
                        (DB-query conn (->sql delete from 'threads (format #f "where id=~a" threadnum))))
                      (begin
                        (DB-query conn (->sql delete from (string->symbol (string-append "thread" threadnum)) (format #f "where id=~a" postnum)))
                        (mtable 'set 'threads (format #f "replies=replies-1 where id=~a" threadnum))))
                      (send-text (format #f "\n[0;31mPost [0;32m#~a[0;31m has been successfully deleted from thread [0;32m/~a/~a[0m\n\n" postnum board threadnum))))))))))))