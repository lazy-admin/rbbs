# rbbs #

RBBS (Request-based Bulletin Board System)
is a simple bulletin board, written with Artanis, which can be accessed via terminal emulator using simple tools, and operates on a per-request basis (the web-server model) rather than active login sessions.

## Requirements ##
1) guile
2) guile-dbi
3) guile-artanis

## Setup ##
1) Copy `conf/artanis.conf.template` to `conf/artanis.conf` and edit the settings to your liking
   - In partiular, change the options `host.name`, `host.addr`, and `host.port` to match your server
2) Copy `prv/modules/settings.scm.template` to `prv/modules/settings.scm` and edit it to your liking
3) Run the server by launching `./artanis-server`
